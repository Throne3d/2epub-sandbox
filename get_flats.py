#!/usr/bin/python3
# coding: utf-8
from bs4 import BeautifulSoup
from bs4 import Tag
from bs4 import NavigableString
import urllib
import urllib.parse as urlparse
import re
import os
import subprocess

def findFileFromURL(chapterURL, where="web_cache"):
    if (where != ""): where = where + "/"
    originalChapterURL = chapterURL
    chapterURL = chapterURL.replace("http://", where).replace("https://", where)
    
    urlSplit = urllib.parse.urlsplit(chapterURL)
    queryParams = urllib.parse.parse_qs(urlSplit.query)
    
    fileName = urlSplit.path.split("/")[-1]
    filePath = ("/").join(urlSplit.path.split("/")[:-1]) + "/"
    
    #print("Looking for: {}".format(chapterURL))
    
    foundFile = None # e.g. "513.html?blah=blah" when found
    if (not os.path.isdir(filePath)):
        print("Couldn't find the directory [{}].".format(filePath))
        print("Chapter URL: {}".format(originalChapterURL))
        print("Where: {}".format(where))
    else:
        for item in os.listdir(filePath):
            itemSplit = urllib.parse.urlsplit(item)
            
            if (itemSplit.path == fileName and os.path.isfile(filePath + item)):
                #File has our filename, it's also a file.
                fileParams = urllib.parse.parse_qs(itemSplit.query)
                hasParams = True
                #print("File: {}".format(item))
                for queryParam in queryParams:
                    if (not queryParam in fileParams or not (queryParams[queryParam] in fileParams[queryParam] or fileParams[queryParam] == queryParams[queryParam])):
                        #It doesn't have the current param
                        hasParams = False
                        #print("File is missing param {} or fileParams[queryParam] ({}) doesn't have queryParams[queryParam] ({})".format(queryParam, fileParams[queryParam] if queryParam in fileParams else "Missing", queryParams[queryParam]))
                for fileParam in fileParams:
                    for fileParamValue in fileParams[fileParam]:
                        if (not fileParam in queryParams or not (fileParamValue in queryParams[fileParam] or queryParams[fileParam] == fileParamValue)):
                            hasParams = False #Some parameters are missing from the input for this to be the valid file.
                            #print("File has a param ({}) not in query - either queryParams[fileParam] ({}) is missing or not the same as the file's ({})".format(fileParam, queryParams[fileParam] if fileParam in queryParams else "Missing", fileParamValue))
                if (hasParams):
                    foundFile = filePath + item
        
    #print("Number of files in {}: {}".format(filePath, len(os.listdir(filePath))))
    if not (foundFile):
        print("Couldn't find file [{}] in [{}] (findFileFromURL(\"{}\", \"{}\"))".format(fileName, filePath, originalChapterURL, where))
        #raise FileNotFoundError("Couldn't find file [{}]".format(chapterURL))
        return None
    else:
        return foundFile

def set_param_in_url(url, *args, **kargs):
    if (not len(args) % 2 == 0):
        raise ValueError("Please give a URL then index,value pairs to replace in the query string.")
    
    urlSplit = urllib.parse.urlsplit(url)
    params = urllib.parse.parse_qs(urlSplit.query)
    
    for i in range(0, int(len(args)/2)):
        params[args[i * 2]] = [args[i * 2 + 1]]
    for karg in kargs:
        params[karg] = [kargs[karg]]
    
    new_urlSplit = urlSplit._replace(query=urllib.parse.urlencode(params, doseq=True))
    return urllib.parse.urlunsplit(new_urlSplit)

def get_params_in_url(url):
    urlSplit = urllib.parse.urlsplit(url)
    params = urllib.parse.parse_qs(urlSplit.query)
    
    return params

def get_param_in_url(url, *args):
    urlSplit = urllib.parse.urlsplit(url)
    params = urllib.parse.parse_qs(urlSplit.query)
    
    if (len(args) == 1): 
        if (args[0] in params and len(params[args[0]]) > 0):
            return params[args[0]][0]
        elif args[0] in params:
            return params[args[0]]
        else:
            return None
    ret = []
    for arg in args:
        if (arg in params and len(params[arg]) > 0):
            ret.append(params[arg][0])
        elif (arg in params):
            ret.append(params[arg])
        else:
            ret.append(None)
    return ret

def clear_params_in_url(url):
    urlSplit = urllib.parse.urlsplit(url)
    new_urlSplit = urlSplit._replace(query=[])
    return urllib.parse.urlunsplit(new_urlSplit)
    
def set_url_params(url, *args, **kargs):
    return set_param_in_url(clear_params_in_url(url), *args, **kargs)

#Fetch a file from [url] to the directory [where="web_cache"], showing output if [output],
#generating a message if [notify], and waiting for the download to finish if [wait].
def get_file(url, where="web_cache", output=False, notify=False, wait=True, timestamping=True):
    if (not os.path.isdir(where)):
        os.makedirs(where)
    proc = subprocess.Popen("cd \"" + where + "\" && wget --load-cookies ../wget-cookies --save-cookies ../wget-cookies -w 0.5 --no-verbose --force-directories" + (" --timestamping" if timestamping else "") +" \"" + url + "\"" + (" > /dev/null 2>&1" if not output else ""), shell=True)
    if (wait): 
        proc.wait()
        if (not findFileFromURL(url, where=where)):
            #Retry
            proc = subprocess.Popen("cd \"" + where + "\" && wget --load-cookies ../wget-cookies --save-cookies ../wget-cookies -w 0.5 --force-directories" + (" --timestamping" if timestamping else "") +" \"" + url + "\"", shell=True)
            proc.wait()
            if (not findFileFromURL(url, where=where)):
                raise FileNotFoundError("Couldn't download file [{}]".format(url))
    if (notify): print("Got file {}".format(url))

#Convert the URL [haystack] to an internal URL by replacing all instances of the
#[needles=["http://", "https://"]] to [replace="web_cache/"]
def internalise_url(haystack, replace="web_cache/", needles=["http://", "https://"]):
    if (type(needles) is str): needles = [needle]
    for needle in needles:
        haystack = haystack.replace(needle, replace)
    return haystack

def externalise_url(haystack, replace="http://", needles=["web_cache/"]):
    if (type(needles) is str): needles = [needle]
    for needle in needles:
        haystack = haystack.replace(needle, replace)
    return haystack
    
class ChapterDetails:
    def __init__(self):
        self.url = ""
        self.smallURL = ""
        self.path = ""
        self.name = ""
        self.nameExtras = ""
        self.sectionType = ""
        self.thread = ""
        self.section = ""
        self.sectionExtras = ""
        self.pageCount = 0
    
chapterList = [] #[[web_cache/alicorn.../###.html?view=flat, "Ω ⍋ in defiance of the capitol", "outside help"], ...]
with open("web_cache/tocPages.txt", mode="r") as tocs:
    for toc in tocs:
        if (toc == "" or toc == "\n"):
            continue
        
        toc = toc.replace("\n", "")
        
        print("TOC Page: {}".format(toc))
        #Get a nice BeautifulSoup of the TOC's HTML
        soup = BeautifulSoup(open(findFileFromURL(toc)))
        
        #Get a list of chapter links from each TOC
        entry = soup.select(".entry-content")[0]
        
        #First separator ("<b>SHORT FORM SANDBOXES</b>")
        separators = ["SHORT FORM SANDBOXES", "MULTI-THREAD PLOTS", "IN WHICH I GUEST STAR"]
        storyType = 0
        chapterURL = ""
        chapterName = ""
        chapterExtras = ""
        chapterSection = ""
        chapterSectionExtras = ""
        chapterSectionExtrasDone = False
        sectionType = ""
        cont = False
        for content in entry.contents:
            for separator in separators:
                contentText = (content.text if type(content) is Tag else content if type(content) is NavigableString else "")
                if (contentText.find(separator) != None and contentText.find(separator) != -1):
                    #Ooh! A match!
                    storyType += 1
                    sectionType = separator
                    print("Story type change. (Now in {} - {})".format(storyType, separator))
                    cont = True
            if (cont):
                cont = False
                continue
            
            if (storyType == 1 or storyType == 3):
                #Short form! (or "IN WHICH I GUEST STAR", which is treated the same.)
                if (content.name == "br" or content.name == "a" or (content.name == "strong" and content.find("a"))):
                    #It's a linebreak.
                    #Process the previous content!
                    #print("Linebreak.")
                    if (chapterName != ""):
                        chapterThread = get_param_in_url(chapterURL, "thread") or ""
                        chapterURL = set_url_params(chapterURL, **{"style":"site","view":"flat"})
                        get_file(chapterURL, timestamping=False)
                        chapterExtras = chapterExtras.strip()
                        
                        chapterDetails = ChapterDetails()
                        chapterDetails.url = chapterURL
                        chapterDetails.smallURL = chapterURL.replace("http://", "").replace(".dreamwidth.org", "").replace("?style=site&view=flat", "").replace("?view=flat&style=site", "")
                        chapterDetails.path = findFileFromURL(chapterURL)
                        chapterDetails.name = chapterName
                        chapterDetails.nameExtras = chapterExtras
                        chapterDetails.sectionType = sectionType
                        chapterDetails.thread = chapterThread
                        chapterDetails.section = chapterSection
                        chapterDetails.sectionExtras = chapterSectionExtras
                        chapterList.append(chapterDetails)
                        
                        print("\"{}\": {}".format("{} {}".format(chapterDetails.name, chapterDetails.nameExtras) if chapterDetails.nameExtras != "" else chapterDetails.name, chapterDetails.smallURL))
                        chapterURL = ""
                        chapterName = ""
                        chapterExtras = ""
                if (content.name == "b" or content.name == "u"):
                    #Ooh! A new section.
                    chapterSection = content.text
                    print("New section: {} (story type: {})".format(chapterSection, sectionType))
                elif (content.name == "a"):
                    chapterURL = content["href"].replace("https://", "http://")
                    chapterName = content.text.strip()
                    if (chapterURL.find("dreamwidth.org") == -1):
                        #invalid chapter! (Not dreamwidth - dunno how to handle others yet; blah
                        chapterName = ""
                        chapterURL = ""
                        chapterExtras = ""
                elif (content.name == "strong" and content.find("a")):
                    chapterURL = content.find("a")["href"].replace("https://", "http://")
                    chapterName = content.find("a").text.strip()
                    content.find("a").extract()
                    chapterExtras = content.text.strip()
                elif (chapterURL != ""):
                    chapterExtras += (content.prettify() if type(content) is Tag else content if type(content) is NavigableString else "")
                    #print("Chapter extras...")
            elif (storyType == 2):
                #Multi-thread!
                #print("Blah.")
                if (content.name == "br"):
                    #It's a linebreak.
                    #Process the previous content!
                    #print("Linebreak.")
                    if (chapterURL == "" and chapterSection != "" and not chapterSectionExtrasDone):
                        #Must be on the sectionName.
                        chapterSectionExtrasDone = True
                elif (content.name == "i"):
                    #New section!
                    chapterSection = content.text
                    chapterSectionExtras = ""
                    chapterSectionExtrasDone = False
                elif (content.name == "ol" or content.name == "ul"):
                    chapterSoup = BeautifulSoup(str(content))
                    for link in chapterSoup.select("a"):
                        chapterURL = link["href"].replace("https://", "http://")
                        chapterName = link.text.strip()
                        if (chapterURL.find("dreamwidth.org") == -1):
                            #invalid chapter! (Not dreamwidth - dunno how to handle others yet; blah
                            chapterName = ""
                            chapterURL = ""
                        if (chapterName != "" and chapterURL != ""):
                            if (chapterName != ""):
                                chapterThread = get_param_in_url(chapterURL, "thread") or ""
                                chapterURL = set_url_params(chapterURL, **{"style":"site","view":"flat"})
                                get_file(chapterURL, timestamping=False)
                                chapterExtras = chapterExtras.strip()
                                
                                chapterDetails = ChapterDetails()
                                chapterDetails.url = chapterURL
                                chapterDetails.smallURL = chapterURL.replace("http://", "").replace(".dreamwidth.org", "").replace("?style=site&view=flat", "").replace("?view=flat&style=site", "")
                                chapterDetails.path = findFileFromURL(chapterURL)
                                chapterDetails.name = chapterName
                                chapterDetails.nameExtras = chapterExtras
                                chapterDetails.sectionType = sectionType
                                chapterDetails.thread = chapterThread
                                chapterDetails.section = chapterSection
                                chapterDetails.sectionExtras = chapterSectionExtras
                                chapterList.append(chapterDetails)
                                
                                print("Chapter \"{}\": {}".format("{} ({})".format(chapterDetails.name, chapterDetails.nameExtras) if chapterDetails.nameExtras != "" else chapterDetails.name, chapterDetails.smallURL))
                                chapterURL = ""
                                chapterName = ""
                                chapterExtras = ""
                elif (chapterURL == "" and chapterSection != "" and not chapterSectionExtrasDone):
                    chapterSectionExtras += (content.prettify() if type(content) is Tag else content if type(content) is NavigableString else "")
            #elif (storyType == 3):
                #Guest-star! Treated the same as section 1...
                

#Because dreamwidth is special in how it shows the text, we're flattening stuff and downloading each page of the part.
pageMetas = {} #["alicorn.../###.html?view=flat&style=site"] = [25]
with open("web_cache/flatPageMeta.txt", mode="r") as pageMeta:
    for line in pageMeta:
        if (line == "" or line == "\n"):
            continue
        
        #Get the line's data
        lineAttribs = line.replace("\n", "").split(" # ")
        
        #Set up as: "alicorn.../###.html?view=flat&style=site # 25"
        lineURL = lineAttribs[0]
        lineLastPage = int(lineAttribs[1])
        
        filesExist = True
        if (findFileFromURL("http://" + lineURL)):
            for i in range(1, lineLastPage+1):
                if (not findFileFromURL(set_url_params("http://" + lineURL, **{"page" : i, "style": "site", "view": "flat"}))):
                    filesExist = False
        else:
            filesExist = False
        
        if (filesExist):
            pageMetas[lineURL] = [lineLastPage]
            
        #Since we have all the pages up to and including lineLastPage, we've put it in a list.

#chapterList = [] #[[web_cache/alicorn.../###.html?view=flat&style=site, "Clannish", "Chamomile", PAGECOUNT], ...]

with open("web_cache/flatPageMeta.txt", mode="w") as pageMetaFile:
    print("No message means \"Appears to have no changes before the last page\"")
    print("\"Length changed\" means \"The number of pages appears to have changed\"")
    print("\"Changed\" means \"The 2nd-last page appears to be different\"")
    for chapter in chapterList:
        #findFileFromURL(chapterURL), chapterName, chapterExtras.strip(), sectionType, chapterSection, chapterSectionExtras
        #chapterURL = chapter.url
        #chapterTitle = chapter.name
        #chapterTitleExtras = chapter.nameExtras
        #etc
        
        with open(chapter.path) as chapterFirstPage:
            soup = BeautifulSoup(chapterFirstPage)
            if (soup.find("h3", class_="entry-title") == None):
                print("Error finding the chapter title for {}-{} ({})".format(chapter.section, chapter.name, chapter.smallURL))
                warning = soup.find("h1", class_="text-center")
                if (warning):
                    warningExists = warning.text.find("Discretion Advised")
                    if (warningExists != None and warningExists != -1):
                        print("It's due to a \"Discretion Advised\" warning.")
                        print("Not sure how to fix this at this point - try manually changing the cookies")
                        print("FAILED to load chapter {}-{}.".format(chapter.section, chapter.name))
                        chapter.failed = True
                        continue
            chapter.entryTitle = soup.find("h3", class_="entry-title").get_text()
            chapter.lessbiguousTitle = chapter.entryTitle
            if (re.sub("[^A-Za-z0-9_]", "", chapter.entryTitle).lower().strip() != re.sub("[^A-Za-z0-9_]", "", chapter.name).lower().strip()):
                chapter.lessbiguousTitle = "{} ({})".format(chapter.entryTitle, chapter.name) #less... ambiguous... unambiguous... lessbiguous? :D? whatever. couldn't think of a shorter name.
            print("Processing {}: {}".format(chapter.section, chapter.lessbiguousTitle))
            
            page_list = soup.find("div", class_="comment-page-list")
            if (page_list):
                page_count = int(re.match(r'Page 1 of ([0-9]+)', page_list.find("p").text).groups()[0])
            else:
                page_count = 1
            chapter.pageCount = page_count
            
            chapterLoc = chapter.path.replace("web_cache/", "")
            chapterDone = False
            if (chapterLoc in pageMetas):
                if (page_count == pageMetas[chapterLoc][0] and pageMetas[chapterLoc][0] > 1):
                    #We have the same page-count.
                    #Re-download the 2nd last page, check it's the same, then download the last page.
                    chapterPenultURL = set_url_params("http://" + chapterLoc, **{"page" : pageMetas[chapterLoc][0] - 1, "style": "site", "view": "flat"})
                    get_file(chapterPenultURL, where="temp", timestamping=False)
                    with open(findFileFromURL(chapterPenultURL, where="web_cache/")) as f1:
                        with open(findFileFromURL(chapterPenultURL, where="temp/")) as f2:
                            soup1 = BeautifulSoup(f1)
                            soup2 = BeautifulSoup(f2)
                            if (str(soup1.select("div#content")[0]) == str(soup2.select("div#content")[0])):
                                #if (f1.read() == f2.read()):
                                #It's the same!
                                chapterDone = True
                                #Don't bother downloading the whole thing
                                #Just get the latest chapter, since it's possible we have
                                #the right number of pages, and the 2nd last page is the
                                #same, but the most recent page is different.
                                get_file(set_url_params("http://" + chapterLoc, **{"page" : pageMetas[chapterLoc][0], "style": "site", "view": "flat"}), timestamping=False)
                                #print("Unchanged.")
                            else:
                                print("Changed.")
                                #print("Page web_cache/{} and page temp/{} are different?".format(chapterPenultURL, chapterPenultURL))
                                #different therefore download it.
                elif (page_count == pageMetas[chapterLoc][0]):
                    print("This chapter seems to only have one page.")
                else:
                    print("Length changed.")
                    print("Prev page count: {}/{}".format(pageMetas[chapterLoc][0], page_count))
                    #Different count! Re-download them all.
                    #chapterDone = False #still
                    
            
            if (not chapterDone):
                for i in range(1, page_count+1):
                    get_file(set_url_params("http://" + chapterLoc, **{"page" : i, "style": "site", "view": "flat"}), timestamping=False)
                print("Downloaded {} pages.".format(page_count))
            
            pageMetas[chapterLoc] = [page_count]
            pageMetaFile.write("{} # {}\n".format(chapterLoc, str(page_count)))
            pageMetaFile.flush()

#Now let's make the files so parse_all_flats_epub.py knows what it's got to parse.
with open("web_cache/chapterList.txt", mode="w") as chapterListFile:
    with open("web_cache/chapterNames.txt", mode="w") as chapterNamesFile:
        for chapter in chapterList:
            if (not (hasattr(chapter, "failed") and chapter.failed)):
                chapterListFile.write("{}\n".format(chapter.url))
                chapterNamesFile.write("{} ~#~ {} ~#~ {} ~#~ {} ~#~ {} ~#~ {}\n".format(chapter.sectionType, chapter.section, chapter.sectionExtras, chapter.name, chapter.nameExtras, chapter.thread))
            else:
                #The chapter somehow failed
                print("Error with chapter {}-{} ({})".format(chapter.section, chapter.name, chapter.smallURL))